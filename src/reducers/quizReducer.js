import * as actionTypes from '../actions/types';
import * as themes from '../constants/themes';

const initialState = {
  // TODO: (cleanup) Move this into another reducer
  currentInstrument: '',
  theme: themes.none,

  // Quiz Tracking
  quiz: [],
  currentQuestionIndex: -1, // -1 indicates no question yet
  questionsAnswered: 0,
  correctAnswers: 0,
  missedNotes: [],
};

const quizReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_THEME:
      return {...state, theme: action.payload};

    case actionTypes.SET_INSTRUMENT:
      return {...state, currentInstrument: action.payload};

    case actionTypes.SET_QUIZ:
      return {
        ...state,
        quiz: action.payload,
        currentQuestionIndex: 0,
        questionsAnswered: 0,
        correctAnswers: 0,
        missedNotes: [],
      };

    case actionTypes.SAVE_ANSWER:
      const {correct, noteData, selection} = action.payload;
      const updatedMissedNotes = correct
        ? state.missedNotes
        : [...state.missedNotes, {noteData, selection}];
      return {
        ...state,
        questionsAnswered: state.questionsAnswered + 1,
        correctAnswers: state.correctAnswers + (correct ? 1 : 0),
        missedNotes: updatedMissedNotes,
      };

    case actionTypes.NEXT_QUESTION:
      return {...state, currentQuestionIndex: state.currentQuestionIndex + 1};

    default:
      return state;
  }
};

export default quizReducer;

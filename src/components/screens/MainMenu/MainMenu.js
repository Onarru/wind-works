import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  SafeAreaView,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';

import {generateQuiz} from '../../../helpers/quizFunctions';
import {setTheme, setInstrument, setQuiz} from '../../../actions/quizActions';
import * as instruments from '../../../constants/instruments';
import * as colors from '../../../constants/colors';
import * as themes from '../../../constants/themes';
import styles from './MainMenu.styles';

// TEMP
const fluteImage = require('../../../images/flute.jpeg');
const oboeImage = require('../../../images/oboe.jpeg');
const clarinetImage = require('../../../images/clarinet.jpeg');
const saxImage = require('../../../images/alto_sax.jpeg');
const bassoonImage = require('../../../images/bassoon.jpeg');

class MainMenu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.scrollContent}
        showsVerticalScrollIndicator={false}>
        {this.renderButton('FLUTE', fluteImage, colors.FLUTE, () =>
          this.onSelection(instruments.FLUTE, themes.flute),
        )}
        {this.renderButton('OBOE', oboeImage, colors.OBOE, () =>
          this.onSelection(instruments.OBOE, themes.oboe),
        )}
        {this.renderButton('BASSOON', bassoonImage, colors.BASSOON, () =>
          this.onSelection(instruments.BASSOON, themes.bassoon),
        )}
        {this.renderButton('CLARINET', clarinetImage, colors.CLARINET, () =>
          this.onSelection(instruments.CLARINET, themes.clarinet),
        )}
        {this.renderButton('ALTO SAX', saxImage, colors.ALTO_SAX, () =>
          this.onSelection(instruments.ALTO_SAX, themes.altoSax),
        )}
      </ScrollView>
    );
  }

  renderButton(label, image, color, onPress) {
    return (
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.buttonContainer}>
          <ImageBackground
            source={image}
            style={styles.buttonImage}
            resizeMode="cover">
            <View
              style={{...styles.buttonTextContainer, backgroundColor: color}}>
              <Text style={styles.buttonText}>{label}</Text>
            </View>
          </ImageBackground>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  // ----- Helper Functions -----
  onSelection(instrument, newTheme) {
    this.props.setTheme(newTheme);
    this.props.setInstrument(instrument);
    this.props.setQuiz(generateQuiz(instrument, 10));

    // TODO: Insert animation delay here
    this.props.navigation.navigate('quiz', {instrument});
  }
}

export default connect(
  null,
  {setTheme, setInstrument, setQuiz},
)(MainMenu);

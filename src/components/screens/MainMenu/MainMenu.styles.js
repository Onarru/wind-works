import {Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import * as colors from '../../../constants/colors';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: colors.GREY,
  },
  scrollContent: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: '10@ms',
    paddingBottom: '20@ms',
  },
  buttonContainer: {
    width: deviceWidth * 0.95,
    height: '125@ms',
    marginTop: '10@ms',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    borderColor: 'green',
    borderRadius: 10,
    // Shadow - ios
    shadowOpacity: 0.4,
    shadowRadius: 3,
    shadowOffset: {
      height: 2,
      width: 2,
    },
    // Shadow - android
    elevation: 1,
  },
  buttonImage: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    borderRadius: 10,
    overflow: 'hidden',
  },
  buttonTextContainer: {
    width: '100%',
    height: '40%',
    backgroundColor: 'black',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  buttonText: {
    marginLeft: '10@ms',
    color: 'white',
    fontSize: '32@ms',
    fontWeight: 'bold',
    textAlignVertical: 'bottom',
  },
});

export default styles;

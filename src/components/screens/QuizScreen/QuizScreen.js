import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  Dimensions,
  Animated,
  Easing,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {connect} from 'react-redux';

import {saveAnswer, nextQuestion} from '../../../actions/quizActions';
import * as colors from '../../../constants/colors';
import PitchSelector from './PitchSelector';

// DEBUG
const testNote = require('../../../images/notes/C4.png');

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

class QuizScreen extends Component {
  // ----- Lifecycle Functions -----
  constructor(props) {
    super(props);
    this.state = {
      allowInput: true,
      animation: new Animated.Value(0),
      loading: true,
    };

    // Bind functions for efficiency
    this.onAnswer = this.onAnswer.bind(this);
  }

  componentDidUpdate(prevProps) {
    // Check if the question has
  }

  // ----- Render Functions -----
  render() {
    const {theme, currentQuestion, questionNumber} = this.props;
    const pointerEvents = this.state.allowInput ? 'auto' : 'none';

    // Error checking
    if (!this.props.quiz || this.props.currentQuestionIndex === -1) {
      console.warn('Missing quiz or question in quizReducer');
      return <></>;
    }

    return (
      <SafeAreaView style={{backgroundColor: colors.GREY}}>
        <View style={styles.container} pointerEvents={pointerEvents}>
          {this.renderInstructions()}
          {this.renderNote()}
          <PitchSelector
            onAnswer={this.onAnswer}
            theme={theme}
            answer={currentQuestion.noteData.pitch}
            questionNumber={questionNumber}
          />
        </View>
      </SafeAreaView>
    );
  }

  renderInstructions() {
    return (
      <View style={styles.instructionsContainer}>
        <Text style={styles.instructionsText}>
          What is the pitch tendency of the note below?
        </Text>
      </View>
    );
  }

  renderNote() {
    const {currentQuestion} = this.props;
    return (
      <View style={styles.noteContainer}>
        <Image
          source={currentQuestion.noteData.img}
          style={styles.noteImage}
          resizeMode="contain"
        />
      </View>
    );
  }

  // ----- Helper Functions -----
  onAnswer(answer) {
    // Determine if the answer was correct and report
    const {currentQuestion, quiz, questionNumber} = this.props;

    // Disable input and begin animation
    this.setState({allowInput: false});

    // TODO: Do this later
    this.props.saveAnswer({
      noteData: currentQuestion.noteData,
      correct: answer === currentQuestion.noteData.pitch,
      selection: answer,
    });

    setTimeout(() => {
      if (questionNumber === quiz.length - 1) {
        this.props.navigation.navigate('results');
      } else {
        this.props.nextQuestion();
        this.setState({allowInput: true});
      }
    }, 1000);
  }
}

const styles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: colors.GREY,
  },
  instructionsContainer: {
    width: deviceWidth,
    height: deviceHeight * 0.1,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  instructionsText: {
    fontSize: '28@ms',
    color: 'black',
    textAlign: 'center',
  },
  noteContainer: {
    width: deviceWidth,
    height: deviceHeight * 0.35,
    backgroundColor: 'white',
  },
  noteImage: {
    height: '100%',
    width: '100%',
  },
});

const mapStateToProps = state => {
  const currentQuestion =
    state.quiz.currentQuestionIndex === -1
      ? {}
      : state.quiz.quiz[state.quiz.currentQuestionIndex];

  return {
    quiz: state.quiz.quiz,
    questionNumber: state.quiz.currentQuestionIndex,
    currentQuestion,
    theme: state.quiz.theme,
  };
};

export default connect(
  mapStateToProps,
  {saveAnswer, nextQuestion},
)(QuizScreen);

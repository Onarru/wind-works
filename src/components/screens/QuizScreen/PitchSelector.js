import React from 'react';
import {View, TouchableOpacity, Text, Dimensions, Animated} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import * as colors from '../../../constants/colors';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const PitchSelector = ({onAnswer, theme, answer, questionNumber}) => {
  const [selection, setSelection] = React.useState(null);
  React.useEffect(() => {
    setSelection(null);
  }, [questionNumber]);

  // Determine what color the sharp button is
  let sharpColor = theme.primary;
  if (selection) {
    if (selection === 'SHARP') {
      sharpColor = answer === 'SHARP' ? colors.CORRECT : colors.INCORRECT;
    } else if (answer === 'SHARP') {
      sharpColor = colors.CORRECT;
    }
  }

  // Determine what color the ok button is
  let okColor = theme.primary;
  if (selection) {
    if (selection === 'OK') {
      okColor = answer === 'OK' ? colors.CORRECT : colors.INCORRECT;
    } else if (answer === 'OK') {
      okColor = colors.CORRECT;
    }
  }

  // Determine what color the flat button is
  let flatColor = theme.primary;
  if (selection) {
    if (selection === 'FLAT') {
      flatColor = answer === 'FLAT' ? colors.CORRECT : colors.INCORRECT;
    } else if (answer === 'FLAT') {
      flatColor = colors.CORRECT;
    }
  }

  return (
    <View style={styles.container}>
      {/* Sharp Button */}
      <TouchableOpacity
        onPress={() => {
          setSelection('SHARP');
          onAnswer('SHARP');
        }}>
        <View style={{...styles.buttonContainer, backgroundColor: sharpColor}}>
          <Text style={styles.buttonText}>Sharp</Text>
        </View>
      </TouchableOpacity>

      {/* OK Button */}
      <TouchableOpacity
        onPress={() => {
          setSelection('OK');
          onAnswer('OK');
        }}>
        <View style={{...styles.buttonContainer, backgroundColor: okColor}}>
          <Text style={styles.buttonText}>{'OK'}</Text>
          <Text style={styles.buttonTextSmall}>{'(In Tune)'}</Text>
        </View>
      </TouchableOpacity>

      {/* Flat Button */}
      <TouchableOpacity
        onPress={() => {
          setSelection('FLAT');
          onAnswer('FLAT');
        }}>
        <View style={{...styles.buttonContainer, backgroundColor: flatColor}}>
          <Text style={styles.buttonText}>Flat</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    width: deviceWidth * 0.95,
    height: deviceHeight * 0.4,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: '10@ms',
    backgroundColor: colors.GREY,
  },
  buttonContainer: {
    width: deviceWidth * 0.95,
    height: deviceHeight * 0.4 * 0.25, // 1/4th of the container height
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: '10@ms',
    borderRadius: 15,
    backgroundColor: colors.OK_BUTTON,
    // Shadow - ios
    shadowOpacity: 0.4,
    shadowRadius: 3,
    shadowOffset: {
      height: 2,
      width: 2,
    },
    // Shadow - android
    elevation: 1,
  },
  sharpContainer: {
    height: '100%',
    width: deviceWidth * 0.95 * 0.3,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: colors.SHARP_BUTTON,
  },
  buttonText: {
    fontSize: '32@ms',
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  buttonTextSmall: {
    fontSize: '20@ms',
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
});

export default PitchSelector;

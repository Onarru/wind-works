import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import * as colors from '../../../constants/colors';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const logo = require('../../../images/logo.png');

class SplashScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {visible: true};
  }

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <Image source={logo} style={styles.logo} resizeMode="contain" />
        </View>
        <View style={styles.buttonContaienr}>
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('selectInstrument')}>
            <View style={styles.nextButton}>
              <Text style={styles.buttonText}>Get Started</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.bottomContainer}>
          <Text style={styles.creditsText}>Project by: Adam Gresham for The American Band College of Central Washington University</Text>
        </View>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    width: deviceWidth,
    height: deviceHeight,
    backgroundColor: colors.DEFAULT,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  topContainer: {
    width: deviceWidth,
    height: deviceHeight * 0.4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContaienr: {
    width: deviceWidth,
    height: deviceHeight * 0.3,
    paddingTop: '150@ms',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomContainer: {
    width: deviceWidth,
    height: deviceHeight * 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  nextButton: {
    height: '75@ms',
    margin: '10@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
  },
  buttonText: {
    color: colors.DEFAULT,
    fontSize: '32@ms',
    fontWeight: 'bold',
    marginHorizontal: '20@ms',
  },
  logo: {
    width: '70%',
    height: '70%',
  },
  loadingAnimation: {
    marginTop: '50@ms',
    width: '300@ms',
    height: '300@ms',
  },
  creditsText: {
    fontSize: '14@ms',
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: '30@ms',
  },
});

export default SplashScreen;

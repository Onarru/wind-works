import {Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import * as colors from '../../../constants/colors';

const deviceHeight = Dimensions.get('window').height;

const styles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'white',
  },
  titleContainer: {
    height: '50@ms',
    margin: '20@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: colors.DEFAULT,
  },
  chartContainer: {
    width: '100%',
    height: deviceHeight * 0.25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backContainer: {
    width: '100%',
    height: deviceHeight * 0.1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backButton: {
    height: '40@ms',
    margin: '10@ms',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: colors.DEFAULT,
  },
  chart: {
    width: '175@ms',
    height: '60%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    color: 'white',
    marginHorizontal: '10@ms',
    fontWeight: '800',
    fontSize: '32@ms',
  },
  tableHeader: {
    width: '100%',
    height: '30@ms',
    marginTop: '5@ms',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
  },
  headerSection: {
    width: '37.5%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitleText: {
    fontWeight: '600',
    fontSize: '28@ms',
    marginTop: '5@ms',
    color: '#717171',
    textAlign: 'center',
    backgroundColor: 'white',
  },
  headerText: {
    fontSize: '20@ms',
    textAlign: 'center',
    color: '#717171',
  },
  headerBar: {
    width: '90%',
    height: 2,
    backgroundColor: '#717171',
  },
  resultsText: {
    fontSize: '32@ms',
    fontWeight: 'bold',
    color: colors.CORRECT,
  },
});

export default styles;

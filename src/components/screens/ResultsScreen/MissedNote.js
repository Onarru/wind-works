import React from 'react';
import {View, Text} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import * as colors from '../../../constants/colors';

const MissedNote = ({missedNote, theme}) => {
  const {noteData, selection} = missedNote;
  return (
    <View style={{...styles.container, backgroundColor: theme.light}}>
      <View style={{...styles.nameContainer, backgroundColor: theme.primary}}>
        <Text style={styles.text}>{noteData.text}</Text>
      </View>
      <View style={styles.infoContainer}>
        <Text style={styles.text}>{selection}</Text>
      </View>
      <View style={styles.infoContainer}>
        <Text style={styles.text}>{noteData.pitch}</Text>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    width: '90%',
    height: '75@ms',
    borderRadius: '10@ms',
    backgroundColor: colors.DEFAULT,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: '15@ms',
  },
  nameContainer: {
    width: '25%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: '10@ms',
    borderBottomLeftRadius: '10@ms',
    backgroundColor: colors.GREY,
  },
  infoContainer: {
    width: '37.5%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: '26@ms',
    color: 'white',
    fontWeight: 'bold',
  },
});

export default MissedNote;

import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import {connect} from 'react-redux';
import {AnimatedCircularProgress} from 'react-native-circular-progress';

import MissedNote from './MissedNote';
import * as colors from '../../../constants/colors';
import styles from './ResultsScreen.styles';

class ResultsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {percentage: 0};
  }

  render() {
    const {
      questionsAnswered,
      correctAnswers,
      currentInstrument,
      theme,
    } = this.props;
    const percentCorrect = correctAnswers / questionsAnswered;
    console.log(percentCorrect);
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{...styles.titleContainer, backgroundColor: theme.primary}}>
          <Text style={styles.titleText}>{currentInstrument}</Text>
        </View>
        <View style={styles.chartContainer}>
          <AnimatedCircularProgress
            size={styles.chart.width}
            width={30}
            rotation={180}
            fill={Math.floor(percentCorrect * 100)}
            prefill={0}
            tintColor={colors.CORRECT}>
            {fill => {
              return (
                <Text style={styles.resultsText}>{`${Math.floor(fill)}%`}</Text>
              );
            }}
          </AnimatedCircularProgress>
        </View>
        <Text style={{...styles.headerTitleText, color: theme.dark}}>Missed Notes</Text>
        <View style={{...styles.headerBar, color: theme.dark}} />
        <View style={styles.tableHeader}>
          <View style={{...styles.headerSection, width: '25%'}}>
            <Text style={styles.headerText}>Note</Text>
          </View>
          <View style={styles.headerSection}>
            <Text style={styles.headerText}>Your Choice</Text>
          </View>
          <View style={styles.headerSection}>
            <Text style={styles.headerText}>Correct</Text>
          </View>
        </View>
        {this.renderNoteList()}
        <View style={styles.backContainer}>
          <View style={{...styles.backButton, backgroundColor: theme.light}}>
            <TouchableWithoutFeedback
              onPress={() => this.props.navigation.navigate('selectInstrument')}>
              <Text style={styles.titleText}>Back</Text>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </SafeAreaView>
    );
  }

  renderNoteList() {
    const {missedNotes} = this.props;
    return (
      <FlatList
        style={{backgroundColor: colors.GREY, width: '100%'}}
        contentContainerStyle={{alignItems: 'center'}}
        data={missedNotes}
        renderItem={({item}) => this.renderDifficultNote(item)}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }

  renderDifficultNote(item) {
    return <MissedNote missedNote={item} theme={this.props.theme} />;
  }
}

const mapStateToProps = state => ({
  theme: state.quiz.theme,
  currentInstrument: state.quiz.currentInstrument,
  questionsAnswered: state.quiz.questionsAnswered,
  correctAnswers: state.quiz.correctAnswers,
  missedNotes: state.quiz.missedNotes,
});

export default connect(
  mapStateToProps,
  null,
)(ResultsScreen);

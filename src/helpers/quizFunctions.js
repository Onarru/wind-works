import * as instruments from '../constants/instruments';
// TODO: BASSOON!
import {
  fluteNotes,
  clarinetNotes,
  altoSaxNotes,
  oboeNotes,
  bassoonNotes,
  noteImages,
} from '../data';

const QUESTION_TEXT = 'What is the pitch tendency of the note below?';

export const generateQuiz = (instrument, questionCount) => {
  const quiz = [];
  const noteLookup = getNotes(instrument);
  const notes = Object.keys(noteLookup);
  const selectedNotes = {};

  // Prevent infinite loop
  if (questionCount > notes.length) {
    console.warn(
      'Cannot generate quiz with question count larger than supported notes for this instrument',
    );
    return quiz;
  }

  for (let i = 0; i < questionCount; i++) {
    // Get a unique note so we don't repeat
    let randomNote = getRandomElement(notes);
    while (selectedNotes[randomNote]) {
      randomNote = getRandomElement(notes);
    }
    selectedNotes[randomNote] = true;

    quiz.push({
      text: QUESTION_TEXT,
      noteData: noteLookup[randomNote], // Object that includes answer
      image: noteImages[randomNote],
    });
  }
  
  return quiz;
};

// ---- Helpers ----
const getRandomElement = arr => {
  return arr[Math.floor(Math.random() * arr.length)];
};

const getNotes = instrument => {
  switch (instrument) {
    case instruments.FLUTE:
      return fluteNotes;
    case instruments.CLARINET:
      return clarinetNotes;
    case instruments.ALTO_SAX:
      return altoSaxNotes;
    case instruments.OBOE:
      return oboeNotes;
    case instruments.BASSOON:
      return bassoonNotes;
  }
};

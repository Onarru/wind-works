import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducers';

// ----- Store functonality -----
let store;

export function dispatch(action) {
  store.dispatch(action);
}

export function getState() {
  return store.getState();
}

export function setStore(newStore) {
  store = newStore;
}

export function getStore() {
  return store;
}

// ----- Store Setup -----
const getMiddleware = () => applyMiddleware(thunk);

export const configureStore = initialState => {
  const newStore = createStore(rootReducer, initialState, getMiddleware());
  setStore(newStore);
  return newStore;
};

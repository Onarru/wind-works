import * as actionTypes from './types';

export const setInstrument = instrument => ({
  type: actionTypes.SET_INSTRUMENT,
  payload: instrument,
});

export const setTheme = theme => ({
  type: actionTypes.SET_THEME,
  payload: theme,
});

export const setQuiz = quiz => ({
  type: actionTypes.SET_QUIZ,
  payload: quiz,
});

export const saveAnswer = answer => ({
  type: actionTypes.SAVE_ANSWER,
  payload: answer,
});

export const nextQuestion = () => ({
  type: actionTypes.NEXT_QUESTION,
});

export const endQuiz = () => ({
  type: actionTypes.END_QUIZ,
});

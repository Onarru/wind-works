// ----- Quiz Actions -----
export const SET_INSTRUMENT = 'SET_INSTRUMENT';
export const SET_THEME = 'SET_THEME';
export const SET_QUIZ = 'SET_QUIZ';
export const SAVE_ANSWER = 'SAVE_ANSWER';
export const NEXT_QUESTION = 'NEXT_QUESTION';
export const END_QUIZ = 'END_QUIZ';

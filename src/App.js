/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Provider} from 'react-redux';

import {configureStore} from './Store';
import Router from './Router';

const App: () => React$Node = () => {
  return (
    <Provider store={configureStore()}>
      <Router
        ref={nav => {
          this.navigator = nav;
        }}
      />
    </Provider>
  );
};

export default App;

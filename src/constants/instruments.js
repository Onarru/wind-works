export const FLUTE = 'Flute';
export const CLARINET = 'Clarinet';
export const ALTO_SAX = 'Alto Saxophone';
export const BASSOON = 'Bassoon';
export const OBOE = 'Oboe';

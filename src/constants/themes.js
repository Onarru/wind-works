import * as colors from './colors';
import * as instruments from './instruments';

export const none = {
  primary: colors.DEFAULT,
  dark: colors.DEFAULT,
  light: colors.DEFAULT,
  background: colors.GREY,
};

export const flute = {
  primary: colors.FLUTE,
  dark: colors.FLUTE_DARK,
  light: colors.FLUTE_LIGHT,
  background: colors.GREY,
};

export const oboe = {
  primary: colors.OBOE,
  dark: colors.OBOE_DARK,
  light: colors.OBOE_LIGHT,
  background: colors.GREY,
};

export const clarinet = {
  primary: colors.CLARINET,
  dark: colors.CLARINET_DARK,
  light: colors.CLARINET_LIGHT,
  background: colors.GREY,
};

export const bassoon = {
  primary: colors.BASSOON,
  dark: colors.BASSOON_DARK,
  light: colors.BASSOON_LIGHT,
  background: colors.GREY,
};

export const altoSax = {
  primary: colors.ALTO_SAX,
  dark: colors.ALTO_SAX_DARK,
  light: colors.ALTO_SAX_LIGHT,
  background: colors.GREY,
};

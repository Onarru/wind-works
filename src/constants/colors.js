export const GREY = '#e5e5e5';
export const DEFAULT = '#05668d';

// Quiz Buttons
export const FLAT_BUTTON = 'red';
export const OK_BUTTON = 'green';
export const SHARP_BUTTON = 'yellow';

export const INCORRECT = '#cc0000';
export const CORRECT = '#00cc00';

// ----- Instuments -----
export const FLUTE_DARK = '#0d82a5';
export const FLUTE = '#4CC9F0';
export const FLUTE_LIGHT = '#a1e3f7';

export const OBOE_DARK = '#0f2aa3';
export const OBOE = '#4361EE';
export const OBOE_LIGHT = '#a2b1f6';

export const BASSOON_DARK = '#190548';
export const BASSOON = '#3A0CA3';
export const BASSOON_LIGHT = '#7640f2';

export const ALTO_SAX_DARK = '#7c043a';
export const ALTO_SAX = '#F72585';
export const ALTO_SAX_LIGHT = '#fb84b9';

export const CLARINET_DARK = '#5a0792';
export const CLARINET = '#7209B7';
export const CLARINET_LIGHT = '#b655f7';

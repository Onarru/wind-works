import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

// Screens
import SplashScreen from './components/screens/Splash';
import MainMenu from './components/screens/MainMenu';
import QuizScreen from './components/screens/QuizScreen';
import ResultsScreen from './components/screens/ResultsScreen';

import {getStore} from './Store';
import * as colors from './constants/colors';

const styles = {
  header: {
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    shadowColor: 'transparent',
    backgroundColor: colors.DEFAULT,
  },
};

const mainNavigator = createStackNavigator({
  splash: {
    screen: SplashScreen,
    navigationOptions: ({navigation}) => {
      return {
        gestureEnabled: false,
        title: '',
        headerStyle: styles.header,
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontSize: 20,
        },
        headerLeft: () => null, // Don't let users go back to the splash
      };
    },
  },
  selectInstrument: {
    screen: MainMenu,
    navigationOptions: ({navigation}) => {
      return {
        gestureEnabled: false,
        title: 'Select Instrument',
        headerStyle: styles.header,
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontSize: 20,
        },
        headerLeft: () => null, // Don't let users go back to the splash
      };
    },
  },
  quiz: {
    screen: QuizScreen,
    mode: 'modal',
    navigationOptions: ({navigation}) => {
      return {
        gestureEnabled: false,
        title: getStore().getState().quiz.currentInstrument,
        headerStyle: {
          ...styles.header,
          backgroundColor: getStore().getState().quiz.theme.primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontSize: 20,
        },
        headerBackTitle: 'Back',
      };
    },
  },
  results: {
    screen: ResultsScreen,
    navigationOptions: ({navigation}) => {
      return {
        gestureEnabled: false,
        header: () => null,
      };
    },
  },
});

const Router = createAppContainer(mainNavigator);
export default Router;
